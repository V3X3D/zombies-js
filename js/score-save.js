function storageAvailable(type) {
  try {
    var storage = window[type],
      x = '__storage_test__';
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  }
  catch(e) {
    return false;
  }
}
if(storageAvailable('localStorage')) {
  G.saveScores = function() {
    localStorage.setItem('scores', JSON.stringify(G.scores));
  };
  G.loadScores = function() {
    if(localStorage.getItem('scores') !== null)
      G.scores = JSON.parse(localStorage.getItem('scores'));
    for(var i=0; i<G.scores.length; i++)
      if(G.scores[i].highlight)
        G.scores[i].highlight = false;
  };
} else {
  gA.saveScores = function(){};
  gA.loadScores = function(){};
}
