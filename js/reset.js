G.reset = function() {
  "use strict";

  G.kills = 0;
  G.player.reset();
  G.particles.reset();
  G.zombies.reset();
  G.bullets.reset();
  G.drops.reset();

  for(var i=0; i<G.scores.length; i++)
    if(G.scores[i].highlight)
      G.scores[i].highlight = false;
};
