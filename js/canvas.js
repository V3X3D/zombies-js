G.canvas = (function() {
  "use strict";

  var RENDER = function() {
    G.ctx.g.fillStyle = '#755';
    G.ctx.g.fillRect(0, 0, G.CW, G.CH);

    //Player restriciton zone visuals
    G.ctx.g.lineWidth = 4;
    G.ctx.g.fillStyle = '#866';
    G.ctx.g.fillRect(0-G.camera.returnX(), 0-G.camera.returnY(), G.CW, G.CH);
    G.ctx.g.strokeStyle = '#977';
    G.ctx.g.strokeRect(0-G.camera.returnX(), 0-G.camera.returnY(), G.CW, G.CH);
  };

  return { render: RENDER };
})();
