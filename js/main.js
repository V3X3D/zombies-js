(function() {
  "use strict";

  // Global Contained Scope
  window.G = {};
  // Global Variables
  G.TILE = 32;
  G.CW = 20*G.TILE;
  G.CH = 15*G.TILE;

  G.zombieSpawnTimer = 222;
  G.scores = [
    {score: 0, highlight: false},
    {score: 0, highlight: false},
    {score: 0, highlight: false},
    {score: 0, highlight: false},
    {score: 0, highlight: false}
  ];
  G.paused = false;
  G.kills = 0;

  window.onload = function() {
    G.loadScores();

    // Loop functions
    function gameUpdate() {
      if(!G.paused) {
        G.camera.update();
        G.drops.update();
        G.particles.update();
        G.player.update();
        G.zombies.update();
        G.bullets.update();
      }
    }
    function gameRender() {
      G.canvas.render();
      G.drops.render();
      G.particles.render();
      G.player.render();
      G.zombies.render();
      G.bullets.render();

      G.lighting.render();
      G.ui.render();
    }

    function gameLoop() {
      gameUpdate();
      gameRender();
      window.requestAnimationFrame(gameLoop);
    }
    gameLoop();
  };
}());

CanvasRenderingContext2D.prototype.roundRect = function (x, y, w, h, r) {
  if (w < 2 * r) r = w / 2;
  if (h < 2 * r) r = h / 2;
  this.beginPath();
  this.moveTo(x+r, y);
  this.arcTo(x+w, y,   x+w, y+h, r);
  this.arcTo(x+w, y+h, x,   y+h, r);
  this.arcTo(x,   y+h, x,   y,   r);
  this.arcTo(x,   y,   x+w, y,   r);
  this.closePath();
  return this;
};
