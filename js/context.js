G.ctx = (function() {
  "use strict";

  var gameCanvas = document.getElementById('game-canvas'),
    gCtx = gameCanvas.getContext('2d', { alpha: false });

  gameCanvas.width = G.CW;
  gameCanvas.height = G.CH;

  // No anti-aliasing
  gCtx.webkitImageSmoothingEnabled = false;
  gCtx.imageSmoothingEnabled = false;

  return {
    gCanv: gameCanvas,
    g: gCtx
  };
})();
