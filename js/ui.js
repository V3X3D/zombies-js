G.ui = (function() {
  "use strict";

  var alpha = 0,
      alpha2 = 0.30;

  var RENDER = function() {
    G.ctx.g.fillStyle = 'rgba(0,0,0,'+ alpha2 +')';
    G.ctx.g.roundRect(16, 8, G.CW-32, 32, 5);
    G.ctx.g.fill();
    G.ctx.g.font = ''+22+'px monospace';
    G.ctx.g.lineJoin = 'circle';
    G.ctx.g.fillStyle = '#fff';
    G.ctx.g.fillText('Kills: '+G.kills+'', G.CW-G.ctx.g.measureText('Kills: '+G.kills+'').width-42, 32);
    G.ctx.g.fillText('Wep: '+G.player.returnWeapon().name+'', 42, 32);
    G.ctx.g.fillText('HP: '+G.player.returnHp()+'', G.CW/2-G.ctx.g.measureText('HP: '+G.player.returnHp()+'').width/2, 32);

    if(G.player.dead()) {
      if(alpha < 0.75)
        alpha += 0.05;

      if(alpha2 < 0.75)
        alpha2 += 0.03;

      //SCORE BOX AND TEXT
      G.ctx.g.fillStyle = 'rgba(0,0,0,'+ alpha +')';
      G.ctx.g.roundRect(G.CW/2-( G.CW/2.5 )/2, G.CH/2-175, G.CW/2.5, 300, 5);
      G.ctx.g.fill();
      G.ctx.g.font = ''+40+'px monospace';
      G.ctx.g.fillStyle = 'rgba(255,255,255,'+ alpha +')';
      G.ctx.g.fillText('YOU DIED', G.CW/2-G.ctx.g.measureText('YOU DIED').width/2, G.CH/2-130);
      G.ctx.g.fillText('--------', G.CW/2-G.ctx.g.measureText('--------').width/2, G.CH/2-110);

      //SCORES
      G.ctx.g.font = ''+30+'px monospace';
      for(var i=0; i<G.scores.length; i++) {
        var j = i;
        j++;

        if(!G.scores[i].highlight)
          G.ctx.g.fillStyle = 'rgba(255,255,255,'+ alpha +')';
        else
          G.ctx.g.fillStyle = 'rgba(200,200,255,'+ alpha +')';

        G.ctx.g.fillText(''+j+'. Kills: '+G.scores[i].score+'', G.CW/2-G.ctx.g.measureText(''+i+'. Kills: '+G.scores[i].score+'').width/2, G.CH/2-( 80-i*44 ));
      }

      //RESET BOX AND TEXT
      // G.ctx.g.font = ''+30+'px monospace';
      G.ctx.g.fillStyle = 'rgba(0,0,0,'+ alpha +')';
      G.ctx.g.roundRect(G.CW/3, G.CH/2+150, G.CW/3, 50, 5);
      G.ctx.g.fill();
      G.ctx.g.fillStyle = 'rgba(255,255,255,'+ alpha +')';
      G.ctx.g.fillText('R == Reset', G.CW/2-G.ctx.g.measureText('R == Reset').width/2, G.CH/2+185);
    } else if(G.paused) {
      if(alpha < 0.75)
        alpha += 0.1;

      if(alpha2 < 0.75)
        alpha2 += 0.07;


      G.ctx.g.fillStyle = 'rgba(0,0,0,'+ alpha +')';
      G.ctx.g.roundRect(16, G.CH/2-64, G.CW-32, 112, 5);
      G.ctx.g.fill();
      G.ctx.g.font = ''+55+'px monospace';
      G.ctx.g.fillStyle = 'rgba(255,255,255,'+ alpha +')';
      G.ctx.g.fillText('Paused', G.CW/2-G.ctx.g.measureText('Paused').width/2, G.CH/2+8);
    } else if (alpha !== 0) {
      alpha = 0;
      alpha2 = 0.30;
    }

  };

  return {
    render: RENDER
  };
})();
