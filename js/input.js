(function() {
  "use strict";

  var bounds = G.ctx.gCanv.getBoundingClientRect();
  var key, button;

  G.key = {
    left: false,
    up: false,
    right: false,
    down: false
  };

  G.mouse = {
    x: 0,
    y: 0,
    left: false,
    right: false,
    middle: false
  };

  window.onkeydown = function keyDown(e) {
    key = e.keyCode;

    if(key === 65 || key === 37)
      G.key.left = true;
    if(key === 87 || key === 38)
      G.key.up = true;
    if(key === 68 || key === 39)
      G.key.right = true;
    if(key === 83 || key === 40)
      G.key.down = true;
    if(key === 32 && !G.player.dead())
      G.paused = !G.paused;
    if(key === 82 && G.player.dead())
      G.reset();
  };

  window.onkeyup = function keyUp(e) {
    key = e.keyCode;

    if(key === 65 || key === 37)
      G.key.left = false;
    if(key === 87 || key === 38)
      G.key.up = false;
    if(key === 68 || key === 39)
      G.key.right = false;
    if(key === 83 || key === 40)
      G.key.down = false;
  };

  G.ctx.gCanv.oncontextmenu = function(e) { e.preventDefault(); };
  window.onmousedown = function mouseDown(e) {
    button = e.which;
    
    if(button === 1)
      G.mouse.left = true;
    if(button === 2)
      G.mouse.right = true;
    if(button === 3)
      G.mouse.middle = true;
  };
  window.onmouseup = function mouseUp(e) {
    button = e.which;
    
    if(button === 1)
      G.mouse.left = false;
    if(button === 2)
      G.mouse.right = false;
    if(button === 3)
      G.mouse.middle = false;
  };
  window.onmousemove = function(e) {
    G.mouse.x = e.clientX - bounds.left;
    G.mouse.y = e.clientY - bounds.top;
  };

})();

