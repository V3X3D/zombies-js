G.camera = (function() {
  "use strict";
  
  var x = 0,
      y = 0,
      w = G.CW,
      h = G.CH;

  var UPDATE = function() {
    x = G.player.returnXandY().x+G.player.size/2-G.CW/2;
    y = G.player.returnXandY().y+G.player.size/2-G.CH/2;
  };

  var returnX = function() { return x; };
  var returnY = function() { return y; };

  return {
    update: UPDATE,
    returnX: returnX, 
    returnY: returnY 
  };
})();

