G.drops = (function() {
  "use strict";

  var drops = [],
    size = 12,
    i;
    // dropsProto = { color: '#0ff' };

  var CREATE = function(x, y, wep) {
    if(wep === 1) wep = 'Pistol';
    else if(wep === 2) wep = 'Sniper';
    else if(wep === 3) wep = 'Automatic';
    else if(wep === 4) wep = 'Shotgun';
    else if(wep === 5) wep = 'Gatling';
    else if(wep === 6) wep = 'Gravity';
    else wep = undefined;

    if(wep !== undefined) {
      drops.push({
        // __proto__: dropsProto,
        despawn: 350,
        color: G.weapons[wep].dropColor,
        type: wep,
        size: size,
        x: x-size/2,
        y: y-size/2
      });
    }
  };
  var UPDATE = function() {
    for(i=0; i<drops.length; i++)
      if(drops[i].despawn  > 0)
        drops[i].despawn--;
      else if(drops[i].despawn  === 0)
        drops.splice(i, 1);
  };
  var RENDER = function() {
    for(i=0; i<drops.length; i++) {
      G.ctx.g.strokeStyle = '#959';
      G.ctx.g.strokeRect(drops[i].x-G.camera.returnX(), drops[i].y-G.camera.returnY(), size, size);
      G.ctx.g.fillStyle = drops[i].color;
      G.ctx.g.fillRect(drops[i].x-G.camera.returnX(), drops[i].y-G.camera.returnY(), size, size);
    }
  };
  var RESET = function() {
    drops = [];
  };

  function dropsArray() { return drops; }

  return {
    update: UPDATE,
    render: RENDER,
    create: CREATE,
    reset: RESET,
    arr: dropsArray
  };
})();

