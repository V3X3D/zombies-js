G.zombies = (function() {
  "use strict";

  var sizeBase = 26,
    size,
    zombies = [],
    spawnTimer = 0,
    color = '#0d0',
    hp = 50,
    spdMod = 0,
    i;

  var CREATE = function() {
    spawnTimer = G.zombieSpawnTimer;

    size = Math.floor(Math.random()*14)+sizeBase;
    if(size <= 28) {
      color = '#7f0';
      hp = 22;
      spdMod = 0.75;
    } else if(size >= 38) {
      color = '#0c7';
      hp = 78;
      spdMod = -0.25;
    } else {
      color = '#0d0';
      hp = 50;
      spdMod = 0;
    }

    var x = Math.floor(Math.random()*G.CW)-size/2,
        y = Math.floor(Math.random()*G.CH)-size/2,
        random = Math.floor(Math.random()*2),  // Making 0 or 1
        random2 = Math.floor(Math.random()*2); // Making 0 or 1

    // Quick hack to keep them spawning outside the central box.
    // This will later be used to make them spawn past view point.
    // Best way to do this is probably have them spawn on edge of circle.
    if(random) {
      if(random2)
        x = G.CW+G.CW/2;
      else
        x = -size-G.CW/2;
    }else {
      if(random2)
        y = G.CH+G.CH/2;
      else
        y = -size-G.CH/2;
    }

    zombies.push({
      x: x,
      y: y,
      colorDefault: color,
      colorBlink: '#f55',
      color: color,
      size: size,
      spd: ((Math.random()*2)+1)+spdMod,
      hp: hp,
      hit: false,
      attacking: false,
      attackDelay: 55,
      invulnTime: 0,
      knockbackSpd: 0,    //Set when hit
      knockbackSpdDec: 0, //Set when hit
      rotation: 0,        //Set later
      xVel: 0,            //Set later
      yVel: 0             //Set later
    });
  };
  var UPDATE = function() {
    //Creating a zombie when timer is 0
    if(spawnTimer === 0)
      CREATE();
    else if(spawnTimer >= 1)
      spawnTimer--;

    //ZOMBIE LOGIC
    for(i=0; i<zombies.length; i++) {

      if(zombies[i].hit) {
        //Reset if hit while attacking
        zombies[i].attacking = false;
        zombies[i].rotation = 0;
        //Push back if shot
        zombies[i].xVel = zombies[i].knockbackSpd * Math.cos(zombies[i].knockbackAngle);
        zombies[i].yVel = zombies[i].knockbackSpd * Math.sin(zombies[i].knockbackAngle);
        zombies[i].x += zombies[i].xVel;
        zombies[i].y += zombies[i].yVel;

        //Dec the knockback speed to make it look smooth
        if(zombies[i].knockbackSpd > 0)
          zombies[i].knockbackSpd -= zombies[i].knockbackSpdDec;
        else if(zombies[i].knockbackSpd < 0)
          zombies[i].knockbackSpd = 0;

        if(zombies[i] !== undefined) {
          if(zombies[i].invulnTime%5 === 0)
            zombies[i].color = zombies[i].colorBlink;
          if(zombies[i].invulnTime%10 === 0)
            zombies[i].color = zombies[i].colorDefault;

          if(zombies[i].invulnTime > 0) {
            zombies[i].invulnTime--;
          } else if(zombies[i].invulnTime === 0) {
            zombies[i].color = zombies[i].colorDefault;
            zombies[i].hit = false;
          }
        }

      } else {
        if(zombies[i].attacking) {
          if(zombies[i].attackDelay > 0) {
            if(zombies[i].rotation < 90)
              zombies[i].rotation += 6;
            else if(zombies[i].rotation > 90)
              zombies[i].rotation = 90;

            zombies[i].attackDelay--;
          } else if(zombies[i].attackDelay === 0) {
            if(zombies[i] !== undefined) {
              zombies[i].attackDelay = 55;
              zombies[i].rotation = 0;
              zombies[i].attacking = false;
            }
          }
        } else {
          var dx = G.player.returnXandY().x - zombies[i].x,
              dy = G.player.returnXandY().y - zombies[i].y,
              angle = Math.atan2(dy, dx);

          zombies[i].xVel = zombies[i].spd * Math.cos(angle);
          zombies[i].yVel = zombies[i].spd * Math.sin(angle);
          zombies[i].x += zombies[i].xVel;
          zombies[i].y += zombies[i].yVel;
        }
      }

      if(zombies[i].hp <= 0) {
        G.drops.create(zombies[i].x+zombies[i].size/2, zombies[i].y+zombies[i].size/2, Math.floor(Math.random()*10)+1);
        zombies.splice(i, 1);
        G.kills++;
        G.zombieSpawnTimer -= 2; //Spawn faster each kill
      }
    }

  };
  var RENDER = function() {
    for (i=0; i<zombies.length; i++) {
      if(zombies[i].attacking) {
        G.ctx.g.setTransform(1, 0, 0, 1, (zombies[i].x+zombies[i].size/2)-G.camera.returnX(),(zombies[i].y+zombies[i].size/2)-G.camera.returnY());
        G.ctx.g.rotate(zombies[i].rotation*Math.PI/180);
        G.ctx.g.fillStyle = zombies[i].color;
        G.ctx.g.fillRect(-zombies[i].size/2, -zombies[i].size/2, zombies[i].size, zombies[i].size);
        G.ctx.g.setTransform(1,0,0,1,0,0);
      } else {
        G.ctx.g.fillStyle = zombies[i].color;
        G.ctx.g.fillRect(zombies[i].x-G.camera.returnX(), zombies[i].y-G.camera.returnY(), zombies[i].size, zombies[i].size);
      }
    }
  };
  var RESET = function() {
    G.zombieSpawnTimer = 222;
    zombies = [];
    spawnTimer = 0;
  };

  function zombiesArray() { return zombies; }

  return {
    update: UPDATE,
    render: RENDER,
    reset: RESET,
    arr: zombiesArray
  };
})();
