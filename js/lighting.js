G.lighting = (function() {
  "use strict";

  var grd = G.ctx.g.createRadialGradient(G.CW/2,G.CH/2,G.CW/2,G.CW/2,G.CH/2,0);
  grd.addColorStop(0,'rgba(0,0,0,1)');
  grd.addColorStop(1,'rgba(0,0,0,0)');

  var UPDATE = function() {
  };
  var RENDER = function() {
    G.ctx.g.setTransform(1,0,0,1,0,0);
    G.ctx.g.fillStyle = grd;
    G.ctx.g.fillRect(0,0,G.CW,G.CH);
    G.ctx.g.setTransform(1,0,0,1,0,0);
  };

  return {
    update: UPDATE,
    render: RENDER
  };
})();

