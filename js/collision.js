function rectRect(x1, y1, w1, h1, x2, y2, w2, h2) {
  w2 = w2 || w1;
  h2 = h2 || h1;

  if(x1+w1 > x2 && x1 < x2+w2 && y1+h1 > y2 && y1 < y2+h2) return true;
  return false;
}
