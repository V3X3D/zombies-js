G.weapons = {
  Pistol: {
    name: 'Pistol',
    dropColor: '#f99',
    shots: 1,
    spd: 10,
    life: 35,
    size: 8,
    knockbackSpd: 5,
    knockbackSpdDec: 0.5,
    knockbackStun: 150,
    damage: 20,
    delay: 22,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      pX = pX + G.player.size/2;
      pY = pY + G.player.size/2;

      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX),
          vx = Math.cos(angle) * this.spd,
          vy = Math.sin(angle) * this.spd;

      G.bullets.arr().push({
        weapon: this.name,
        cx: pX-this.size/2,
        cy: pY-this.size/2,
        vx: vx,
        vy: vy,
        angle: angle,
        life: this.life
      });
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            if(zombies[j].hit !== true) {
              zombies[j].knockbackAngle = bullets[i].angle;
              zombies[j].knockbackSpd = this.knockbackSpd;
              zombies[j].knockbackSpdDec = this.knockbackSpdDec;
              zombies[j].hp -= this.damage;
              zombies[j].invulnTime = this.knockbackStun/10;
              zombies[j].hit = true;
            }

            G.particles.create(
              bullets[i].cx,
              bullets[i].cy,
              bullets[i].angle,
              this.spd,
              1,
              Math.floor(Math.random()*4)+1,
              'default',
              'default'
            );

            bullets[i].life = 0;
            bullets.splice(i, 1);
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  },
  Sniper: {
    name: 'Sniper',
    dropColor: '#ff0',
    shots: 1,
    spd: 16,
    life: 45,
    size: 14,
    knockbackSpd: 8,
    knockbackSpdDec: 0.5,
    knockbackStun: 350,
    damage: 30,
    delay: 42,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      pX = pX + G.player.size/2;
      pY = pY + G.player.size/2;

      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX),
          vx = Math.cos(angle) * this.spd,
          vy = Math.sin(angle) * this.spd;

      G.bullets.arr().push({
        weapon: this.name,
        cx: pX-this.size/2,
        cy: pY-this.size/2,
        vx: vx,
        vy: vy,
        angle: angle,
        life: this.life
      });
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            if(zombies[j].hit !== true) {
              zombies[j].knockbackAngle = bullets[i].angle;
              zombies[j].knockbackSpd = this.knockbackSpd;
              zombies[j].knockbackSpdDec = this.knockbackSpdDec;
              zombies[j].hp -= this.damage;
              zombies[j].invulnTime = this.knockbackStun/10;
              zombies[j].hit = true;
            }

            G.particles.create(
              bullets[i].cx,
              bullets[i].cy,
              bullets[i].angle,
              this.spd-2,
              1,
              Math.floor(Math.random()*3)+2
            );
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  },
  Automatic: {
    name: 'Automatic',
    dropColor: '#0ff',
    shots: 1,
    spd: 13,
    life: 35,
    size: 8,
    knockbackSpd: 3,
    knockbackSpdDec: 0.3,
    knockbackStun: 70,
    damage: 9,
    delay: 9,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      pX = pX + G.player.size/2;
      pY = pY + G.player.size/2;

      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX),
          invert = Math.random() < 0.5 ? -1 : 1,
          spread = ( Math.floor(Math.random()*4)/30 )*invert,
          vx = Math.cos(angle+spread) * this.spd,
          vy = Math.sin(angle+spread) * this.spd;

      G.bullets.arr().push({
        weapon: this.name,
        cx: pX-this.size/2,
        cy: pY-this.size/2,
        vx: vx,
        vy: vy,
        angle: angle,
        life: this.life
      });
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            if(zombies[j].hit !== true) {
              zombies[j].knockbackAngle = bullets[i].angle;
              zombies[j].knockbackSpd = this.knockbackSpd;
              zombies[j].knockbackSpdDec = this.knockbackSpdDec;
              zombies[j].hp -= this.damage;
              zombies[j].invulnTime = this.knockbackStun/10;
              zombies[j].hit = true;

              G.particles.create(
                bullets[i].cx,
                bullets[i].cy,
                bullets[i].angle,
                this.spd,
                1,
                Math.floor(Math.random()*2)+1
              );

              bullets[i].life = 0;
              bullets.splice(i, 1);
            }
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  },
  Shotgun: {
    name: 'Shotgun',
    dropColor: '#9f9',
    shots: 5,
    spd: 8,
    life: 30,
    size: 8,
    knockbackSpd: 2.5,       //Per bullet gets added to zombies stun
    knockbackSpdDec: 0.1,    //Per bullet gets added to zombies stun
    knockbackStun: 90,       //Per bullet gets added to zombies stun
    damage: 10,
    delay: 38,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX);

      for(var i=0; i<this.shots; i++) {
        pXnew = pX + G.player.size/2;
        pYnew = pY + G.player.size/2;

        var invert = Math.random() < 0.5 ? -1 : 1,
            spread = ( Math.floor(Math.random()*5)/10 )*invert,
            vx = Math.cos(angle+spread) * this.spd,
            vy = Math.sin(angle+spread) * this.spd;

        G.bullets.arr().push({
          weapon: this.name,
          cx: pXnew-this.size/2,
          cy: pYnew-this.size/2,
          vx: vx,
          vy: vy,
          angle: angle,
          life: this.life
        });
      }
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            if(zombies[j].hit !== true)
              zombies[j].knockbackAngle = bullets[i].angle;

            zombies[j].knockbackSpd += this.knockbackSpd;
            zombies[j].knockbackSpdDec += this.knockbackSpdDec;
            zombies[j].invulnTime += this.knockbackStun/10;
            zombies[j].hp -= this.damage;
            zombies[j].hit = true;

            G.particles.create(
              bullets[i].cx,
              bullets[i].cy,
              bullets[i].angle,
              this.spd+2,
              1,
              Math.floor(Math.random()*3)+1
            );

            bullets[i].life = 0;
            bullets.splice(i, 1);
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  },
  Gatling: {
    name: 'Gatling',
    dropColor: '#fa0',
    shots: 1,
    spd: 15,
    life: 25,
    size: 6,
    knockbackSpd: 3,
    knockbackSpdDec: 0.35,
    knockbackStun: 70,
    damage: 5.5,
    delay: 4,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      pX = pX + G.player.size/2;
      pY = pY + G.player.size/2;

      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX),
          invert = Math.random() < 0.5 ? -1 : 1,
          spread = ( Math.floor(Math.random()*7)/30 )*invert,
          vx = Math.cos(angle+spread) * this.spd,
          vy = Math.sin(angle+spread) * this.spd;

      G.bullets.arr().push({
        weapon: this.name,
        cx: pX-this.size/2,
        cy: pY-this.size/2,
        vx: vx,
        vy: vy,
        angle: angle,
        life: this.life
      });
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            if(zombies[j].hit !== true) {
              zombies[j].knockbackAngle = bullets[i].angle;
              zombies[j].knockbackSpd = this.knockbackSpd;
              zombies[j].knockbackSpdDec = this.knockbackSpdDec;
              zombies[j].hp -= this.damage;
              zombies[j].invulnTime = this.knockbackStun/10;
              zombies[j].hit = true;
            }

            G.particles.create(
              bullets[i].cx,
              bullets[i].cy,
              bullets[i].angle,
              this.spd,
              1,
              Math.floor(Math.random()*2)+1
            );

            bullets[i].life = 0;
            bullets.splice(i, 1);
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  },
  Gravity: {
    name: 'Gravity',
    dropColor: '#c9f',
    shots: 1,
    spd: 5,
    life: 24,
    size: 15,
    knockbackSpd: 5,
    knockbackSpdDec: 0.125,
    knockbackStun: 400,
    damage: 28,
    delay: 28,
    create: function(pX, pY) {
      G.bullets.setDelay(this.delay);
      pX = pX + G.player.size/2;
      pY = pY + G.player.size/2;

      var angle = Math.atan2(G.mouse.y+G.camera.returnY() - pY, G.mouse.x+G.camera.returnX() - pX),
          vx = Math.cos(angle) * this.spd,
          vy = Math.sin(angle) * this.spd;


      G.bullets.arr().push({
        weapon: this.name,
        cx: pX-this.size/2,
        cy: pY-this.size/2,
        vx: vx,
        vy: vy,
        angle: angle,
        life: this.life
      });
    },
    update: function(i) {
      var bullets = G.bullets.arr();
      bullets[i].cx += bullets[i].vx;
      bullets[i].cy += bullets[i].vy;

      bullets[i].life--;

      if(bullets[i].life === 0) 
        bullets.splice(i, 1);

      for(var j=0; j<G.zombies.arr().length; j++) {
        var zombies = G.zombies.arr();

        if(bullets[i] !== undefined) {
          if(rectRect(bullets[i].cx, bullets[i].cy, this.size, this.size, zombies[j].x, zombies[j].y, zombies[j].size, zombies[j].size)) {
            // if(zombies[j].hit !== true) {
              zombies[j].knockbackAngle = bullets[i].angle+Math.PI; //Reversing radians by adding half a circle
              zombies[j].knockbackSpd = this.knockbackSpd;
              zombies[j].knockbackSpdDec = this.knockbackSpdDec;
              zombies[j].hp -= this.damage;
              zombies[j].invulnTime = this.knockbackStun/10;
              zombies[j].hit = true;
            // }

            G.particles.create(
              bullets[i].cx,
              bullets[i].cy,
              bullets[i].angle+Math.PI,
              this.spd+2,
              1,
              Math.floor(Math.random()*8)+1
            );

            bullets[i].life = 0;
            bullets.splice(i, 1);
          }
        }
      }

    },
    render: function(i) {
      G.ctx.g.fillStyle = '#fff';
      G.ctx.g.fillRect(G.bullets.arr()[i].cx-G.camera.returnX(), G.bullets.arr()[i].cy-G.camera.returnY(), this.size, this.size);
    }
  }
};
