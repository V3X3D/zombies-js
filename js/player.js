G.player = (function() {
  "use strict";

  var x = G.CW/2-16,
      y = G.CH/2-16,
      size = 32,
      collision = false,
      hp = 100,
      spd = 3,
      colorDefault = '#fc0',
      colorBlink = '#f77',
      color = colorDefault,
      hit = false,
      invulnTime = 0,
      weapon = G.weapons.Pistol,
      dead = false,
      i;

  var UPDATE = function() {
    if(!dead) {
      //Movement
      if(G.key.left)
        x -= spd;
      if(G.key.up)
        y -= spd;
      if(G.key.right)
        x += spd;
      if(G.key.down)
        y += spd;

      //Room Restriction
      if(x < 0)
        x = 0;
      if(y < 0)
        y = 0;
      if(x+size > G.CW)
        x = G.CW-size;
      if(y+size > G.CH)
        y = G.CH-size;

      for(i=0; i<G.drops.arr().length; i++) {
        //Item Drops Collision
        //Fudging the player size a bit
        // console.log(G.drops.arr[i].x);
        if(rectRect(x+2, y+2, size-2, size-2, G.drops.arr()[i].x, G.drops.arr()[i].y, G.drops.arr()[i].size, G.drops.arr()[i].size)) {
          weapon = G.weapons[G.drops.arr()[i].type];
          G.drops.arr().splice(i, 1);
        }
      }

      for(i=0; i<G.zombies.arr().length; i++) {
        var zombies = G.zombies.arr();
        //Zombie Collision
        //Fudging the player size a bit to make things a bit easier for the player
        if(rectRect(x+4, y+4, size-4, size-4, zombies[i].x, zombies[i].y, zombies[i].size, zombies[i].size)) {
          zombies[i].attacking = true;
          if(!hit) {
            hit = true;
            invulnTime = 45;
            color = colorBlink;
            hp -= 20;
            G.particles.create(
              x+size/2,
              y+size/2,
              0,
              5,
              9,
              Math.floor(Math.random()*10)+6,
              Math.floor(Math.random()*180),
              1
            );
            if(hp <= 0) {
              dead = true;
              var newKills = G.kills,
                  oldKills = 0,
                  yourScore = true;

              for(i=0; i<G.scores.length; i++) {
                if(G.scores[i].score < newKills) {
                  oldKills = G.scores[i].score;
                  G.scores[i].score = newKills;
                  newKills = oldKills;

                  if(yourScore) { // Keeps from highlighting everytime something is pushed down
                    G.scores[i].highlight = true;
                    yourScore = false;
                  }
                }
              }
              G.saveScores();
            }

          }
        }
      }
      // Invulnerability timer, also blinking when hit
      if(invulnTime%5 === 0)
        color = colorBlink;
      if(invulnTime%10 === 0)
        color = colorDefault;

      if(invulnTime > 0) {
        invulnTime--;
      } else if(invulnTime === 0) {
        hit = false;
        color = colorDefault;
      }

      //Shooting
      if(G.mouse.left) G.bullets.create(x, y);
    }
  };
  var RENDER = function() {
    if(!dead) {
      G.ctx.g.fillStyle = color;
      G.ctx.g.fillRect(x-G.camera.returnX(), y-G.camera.returnY(), size, size);
    } else {
      G.ctx.g.fillStyle = '#f00';
      G.ctx.g.fillRect(x-G.camera.returnX(), y-G.camera.returnY(), size, size);
    }
  };
  var RESET = function() {
    dead = false;
    x = G.CW/2-16;
    y = G.CH/2-16;
    size = 32;
    collision = false;
    hp = 100;
    spd = 3;
    color = colorDefault;
    hit = false;
    invulnTime = 0;
    weapon = G.weapons.Pistol;
  };

  var returnWeapon = function () { return weapon; },
      returnHp = function() { return hp; },
      returnXandY = function() { return {x: x, y: y}; },
      returnDead = function() { return dead; };

  return {
    update: UPDATE,
    render: RENDER,
    reset: RESET,
    returnWeapon: returnWeapon,
    returnHp: returnHp,
    returnXandY: returnXandY,
    dead: returnDead,
    size: size
  };
})();
