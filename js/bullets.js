G.bullets = (function() {
  "use strict";

  var bullets = [], delay = 0, i;

  var CREATE = function(pX, pY) {
    if(delay === 0)
      G.player.returnWeapon().create(pX, pY);
  };
  var UPDATE = function() {
    if(delay >= 1)
      delay--;

    for(i=0; i<bullets.length; i++)
      G.weapons[""+bullets[i].weapon+""].update(i);
  };
  var RENDER = function() {
    for(i=0; i<bullets.length; i++)
      G.weapons[""+bullets[i].weapon+""].render(i);
  };
  var RESET = function() {
    bullets = [];
    delay = 0;
  };

  function bulletsArray() { return bullets; }
  function setDelay(amount) { delay = amount; }

  return {
    update: UPDATE,
    render: RENDER,
    create: CREATE,
    reset: RESET,
    arr: bulletsArray,
    setDelay: setDelay
  };
})();
