G.particles = (function() {
  "use strict";

  var particles = [],
    i,
    partProtos = {
      blood: {
        w: 6,
        h: 6,
        color: '#f33',
        update: function() {
          var vx = Math.cos(this.angle+this.spread) * this.spd,
              vy = Math.sin(this.angle+this.spread) * this.spd;

          if(this.spd > 0)
            this.spd -= 0.8;
          else
            this.spd = 0;

          this.x += vx;
          this.y += vy;
        },
        render: function() {
          G.ctx.g.fillStyle = this.color;
          G.ctx.g.fillRect(this.x-G.camera.returnX(), this.y-G.camera.returnY(), this.w, this.h);
        }
      }
    };
  var CREATE = function(x, y, angle, minSpd, maxSpd, count, spreadR, spreadD, proto) {
    var spd,
        tmpSpd,
        spreadRange = isNaN(spreadR) ? 5 : spreadR,
        spreadDivisor = isNaN(spreadD) ? 10 : spreadD;

    for(i=0; i<count; i++) {
      var invert = Math.random() < 0.5 ? -1 : 1,
          spread = ( Math.floor(Math.random()*spreadRange)/spreadDivisor )*invert;

      // Setting speed
      if(minSpd !== maxSpd) {
        if(minSpd > maxSpd) {
          maxSpd = minSpd;
          spd = maxSpd;
        } else {
          tmpSpd = maxSpd - minSpd;
          spd = Math.floor(Math.random()*tmpSpd)+minSpd;
        }
      } else {
        spd = maxSpd;
      }

      // Making the particles
      particles.push({
        __proto__: proto || partProtos.blood,
        x: x,
        y: y,
        delay: Math.floor(Math.random()*120)+80,
        invert: invert,
        spread: spread,
        spd: spd,
        angle: angle
      });
    }
  };
  var UPDATE = function() {
    for(i=0; i<particles.length; i++) {
      particles[i].update();

      particles[i].delay--;

      if(particles[i].delay <= 0)
        particles.splice(i, 1);
    }
  };
  var RENDER = function() {
    for(i=0; i<particles.length; i++)
      particles[i].render();
  };
  var RESET = function() { particles = []; };

  function particlesArray() { return particles; }
  function protosArray() { return partProtos; }

  return {
    update: UPDATE,
    render: RENDER,
    create: CREATE,
    reset: RESET,
    arr: particlesArray(),
    protos: protosArray()
  };
})();
