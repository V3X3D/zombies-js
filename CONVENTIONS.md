# Conventions
---
This document contains the conventions used in my standard game projects.
These are for my own reference and reference of others trying to grok a
project I have written.

## Variables Patterns
---
A prefix of an underscore in a variable name notes it is 'private' or should
be looked at as such.
```javascript
  // Private variable prefixed with an '_'.
  var _private = 'Not a publicly exposed string.';
```

A capital cased variable notes that it is a 'constant'.
Thus it shouldn't ever be modified.
```javascript
  // Constant variable fully capitalized.
  var NAME = 'Bobby';
```

Also, 'constant' and 'private' variables can be joined.
```javascript
  // Mix of private and constant in one variable.
  var _JOINED = 10;
```

## Structural Patterns
---
Almost everything is part of a global object, in this case called 'G'.
This keeps variables from being mixed in with default 'window' (global) vars.
```javascript
  // Creation of global object
  window.G = {};
  // Could also be defined in a global scope as the following
  var G = {};
```

Calling/creating should in most cases be done within the 'G' global object.
```javascript
  // Function defined in context of the global object 'G'
  G.player = (function() { console.log(name) })();
```

The function we see defined in the global scope is a self-invoking
function, also know as a immediately-invoked function.
```javascript
  /* Example below shows a comparison between a normal Function
     Declaration, and a Immediately-Invoked Function Expression */

  G.player = function() { console.log(name) };         //Function Declaration
  G.player = (function() { console.log(name) })();     //Immediately-Invoked Function Expression
```

Modular patterns are used to have things similar to 'private' variables. Info
on a modular pattern explained well [Here](https://toddmotto.com/mastering-the-module-pattern/).

Might also try to stick to using pure functions, though I'm not sure at this
point. Info on pure functions [Here](https://toddmotto.com/pure-versus-impure-functions).

## Final Notes
---
Logically global scoped variables cannot be 'private', but they might contain 'private' variables.
Also, global variables can and might often be 'constants'.
