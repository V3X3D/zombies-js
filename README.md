# Zombies JS - A html5 canvas game

## How do I play?

Running the game
- Just clone the repository, and run the `index.html` file in a modern browser.

Play the game (keys, etc)
- Movement:          WASD/Arrows keys.
- Shooting:          Left click.
- Pause:             Space key.
- Restart (if dead): R key.

## TODO

At this point I'm calling the game "finished". Though much more could be done.

I'll leave a list of some things I might add in the future.
- Remove player restriction zone, have it get darker as you go further out, restrict to room size
- Sound, for guns, zombies, etc.
- Screen shake with high power guns
- Canvas scaling
- Intro & Title Screen
- Obstacles for Player and Zombie (would need path finding to go around them).

## Notes

This was never planned to be a full game, so it likely won't bloom into one.
More or less this was just to have some fun and test out some ideas.

## Support

Like this game?

Help me make more by [supporting me on Patreon](https://www.patreon.com/V3X3D) for as little as a dollar each month.
